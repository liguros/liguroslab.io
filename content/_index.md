---
title: 'LiGurOS'
date: 2018-11-28T15:14:39+10:00
---

LiGurOS is a source based distribution building upon code and packages from a broad family of Gentoo-based distributions. Source based means all the software is compiled from source. The Gentoo-family of operating systems heavily uses portage as the package manager with the ability to change settings globally but also per package. So you can specify what exact configuration options are passed to the package upon building. This results in a operating system without bloat and running optimized for your processor.

LiGurOS is optimized for speed and taking advantage of the latest features of AMD and Intel processors. LiGurOS tries to balance between state-of-the-art and stability, providing two basic release models (stable and rolling). As LiGurOS is a semi-/rolling-release distribution, you install the system once and update it throughout the life of your hardware.

LiGurOS gives you the choice of your favorite services manager, out-of-the-box we support openrc, but s6 and others are also an option.

LiGurOS still comes with multilib environment so you can install also 32-bit software (e.g. Steam) on your 64-bit machine without problem. 

Linux Guru's Operating System was created with the need to address some issues that source-based distributions and rolling distributions have. Like transparent automatic testing, semi-rolling release with stability and security in mind just to name a few.

## Security
LiGurOS takes security very seriously. We handle all security problems brought to our attention and ensure that they are corrected within a reasonable timeframe. As of now we are using GLSA (Gentoo Linux Security Advisory) as our base system for announcing and testing for known vulnerabilities.

![GLSA Check after image was build](/images/glsa.png)

Every user can also check if the system is vulnerable to any known security exploits.

![GLSA terminal check](/images/glsa-terminal.png)

## Quality assurance
LiGurOS runs a fully automatic CI (continuous integration) suite that checks the packages added or updated before they hit the end user.

![QA pipeline](/images/pipeline-QA.png)

## Automating development workflow
We try to automate everything through CI/CD workflow. Every commit gets tested in various ways.

![Commit checks](/images/commit-ci.png)
![Commit checks](/images/commit-ci2.png)
![Commit checks](/images/commit-ci3.png)

### Generating install images using GitLab's CI/CD
![Commit checks](/images/subway-running.png)
The whole installation images (stage3 images) are generated automatically according to schedule.

### Portage Ebuilds Tree Regeneration
![Commit checks](/images/merge-scripts-running.png)
Our package repository (portage tree) gets regenerated automatically on each new commit to the repository.

## Download
You can test LiGurOS using LXD or you can install it in a similar way like any \*too distribution.
The files can be downloaded from our [build server](https://build.liguros.net).

## Contributing and bug reporting
You can find the whole project in GitLab at [LiGurOS Project](https://gitlab.com/liguros). New bugs or improvements as well as feature request can be filled in our [issue tracker](https://gitlab.com/liguros/bugs/issues). Please use the search function, before you file new issue.

## Support
We have a dedicated online chat system for asking questions about using the LiGurOS Linux system. 
Please join us on our Zulip Server - a powerfull group chat application - [join group chat](https://chat.liguros.net/join/pdjwvndytkmvcc2muwunjkq5/).



## Donations
We rely on support from people like you to make it possible. If you enjoy using LiGurOS, please consider supporting us by donating and becoming a Patron!
Please check our [donations page](https://liberapay.com/LiGurOS).

We are a small team, so your support makes a huge difference!

------------------------------------------------------------------

Where does the money go?
First of all, powerful servers. We need servers to run all QA and other tasks.

Can I change/cancel my support?
Yes, at any time.

Other methods of donation?
Coming soon.

Are some features reserved to Patrons?
Not yet, but we are planning on providing addon features for supporters.
