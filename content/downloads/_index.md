---
title: 'Downloads'
date: 2018-11-28T15:14:39+10:00
weight: 1
---

## LiGurOS

Is a source-based distribution. Below you can find our archives to download.

| Develop | Stable |
|:---------------------------------------------------------------------------------------------------------:|:---------------------------------------------------------------------------------------------------:|
| <br>Get the newest Linux packages with our rolling release. <br><br>Fast! Integrated! Stabilized! Tested! | <br>Get the most complete Linux distribution <br><br>with LiGurOS's latest regular-release version! |