---
title: 'Breaking changes between v20.7.2 and v21.1.0'
date: 2020-12-16T13:36:37+10:00
weight: 22
---

Are you looking for the new stable release? No worries. Stable release v21.1.0 is already here. Sadly, you can't get it via the normal sync command, as this release includes some breaking changes that make it impossible to automate the update process.

What changes are included in the new release? The main change is the move away from kits. Yes, kits are gone now. The reason for this is simply that we saw no real benefits of using kits anymore. On the contrary, they complicated many things and slowed everything down.

The removal of kits includes a few other changes that are worth mentioning:

1) replacing ego

As ego was designed around kits for some versions, we had to find a replacement. Ripping out the kit parts was not an option, as this would have taken too long and surely broken a lot of things on the way. We could have used the last existing version without kits as a base for our development but with that being version 1.x.x, there was a bunch of current functionality missing. Thus we decided to start from scratch and rewrite ego from scratch in golang.

The new version should contain all the necessary functionality. Some of the old functions were not implemented, as there are better ways to handle these functions now but the basic operations are there. If some of the features you used are missing, please use the issues or IRC to ask for assistance.

Overall, the switch away from kits not only simplified our processes but also improved the speed of the image generation by about a factor of 10.

After all the rambling about the changes, you are probably wondering how you'll be able to get the new version. We have tried to make the process as easy as possible. To update your system you can use this command:
#```
curl https://gitlab.com/liguros/kit-fixups/-/raw/develop/profiles/migrate-repo.sh|bash
#```

This script will do the following:

- Download the new repository
- Update your repository file (in repos.conf) to the new repository values
- Remove the old kit repository definitions
- Update /etc/portage/make.profile/parent
- Emerge the new ego version
- Resync the tree

You only need to update your personal repositories to point to the new masters (liguros-repo instead of core-kit) and reemerge your system.

We hope that you find this post informative and we apologize for making such drastic changes, but that is part of the improvement and advance of our product.

Keep it rolling,
Your LiGurOS Team
