---
title: 'Breaking changes between v19.7.10 and v19.7.11'
date: 2019-11-05T19:29:37+10:00
weight: 22
---

We try hard not to break compatibility between releases, but going forward and implementing new ideas has to pay its price sometimes. With the new release v19.7.11 there are some changes in profiles that will affect your installations.

We have added new mix-ins to select `multilib` and `non-multilib` and so the _pure64 architectures and subarchitectures are going away. Next change has been in naming of the basic architectures. So one of them is renaming of `x86-64bit` => `amd64`. If you are running a 64bit x86 system, you will have to run these commands.

```
ego profile arch amd64
ego profile subarch generic_64 # or any other subarch
```

If you are using multilib implementation you will also need to add `multilib` mix-in.

```
ego profile mix-ins +multilib
```

For those who want to run pure64 systems run the following command:

```
ego profile mix-ins +no-multilib
```

We hope that you find this post informative and we apologize for making such drastic changes, but that is part of improvement and going forward.

Keep it rolling,
Your LiGurOS Team
