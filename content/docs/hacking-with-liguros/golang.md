---
title: "Hacking with LiGurOS - Golang"
date: 2019-11-12T18:48:29+01:00
draft: false
weight: 102
---

This Golang programming tutorial is a great guide to help you get started with programming in Golang (or simply Go) under LiGurOS Linux. We will list more Go tutorials and programming examples to master your knowledge of programming in Go later in this tutorial.

> Go is a programming language designed by Google engineers Robert Griesemer, Rob Pike, and Ken Thompson. Go is a statically typed, compiled language in the tradition of C, with the added benefits of memory safety, garbage collection, structural typing, and CSP-style concurrency. The compiler, tools, and source code are all free and open source. Golang first appeared on November 10, 2009.


Installing Go
-------------

Installation of go in Funtoo Linux is as easy as:

```
liguros ~ # emerge dev-lang/go
```

This will install the latest available version of Go.

Hello from LiGurOS
------------------

```go
package main

import "fmt"

// this is a comment

func main() {
	fmt.Println("Hello from LiGurOS!")
}
```

Now you can run the program with:

```
liguros ~ # go run hello.go
Hello from LiGurOS!
liguros ~ #
```

Useful links for working with Go
--------------------------------

- [Go Tour](https://tour.golang.org) The standard tour of Go to get you started
- [The Zoo of Go Functions](https://blog.learngoprogramming.com/go-functions-overview-anonymous-closures-higher-order-deferred-concurrent-6799008dde7b) An overview of anonymous, higher-order, closures, concurrent, deferred, and other kinds of Golang funcs.
- [PHP >> Golang](https://www.php2golang.com/) Golang alternatives for PHP functions. Useful for porting PHP programs to Go