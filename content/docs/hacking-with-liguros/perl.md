---
title: "Hacking with LiGurOS - Perl"
date: 2019-11-12T18:48:24+01:00
draft: false
weight: 101
---

This Perl programming tutorial is great a help to get you started with programming in Perl under LiGurOS Linux.

> Perl, a high-level, general-purpose, interpreted, dynamic programming language. Perl is noted for its idiomatically rich syntax, its extensive use of regular expressions and the large module archive CPAN.

Perl was created by Larry Wall in 1987. It was originally developed to process text and produce reports . As a result, a backronym has been formed from its name:
Practical Extraction and Report Language.

Installing Perl
---------------

LiGurOS Linux stages come with Perl included. The next command is only needed if you want to make sure you have the latest available version of Perl installed.

```
liguros ~ # emerge dev-lang/perl
```

You can check which version of Perl is installed by running: `perl -v`


Hello from LiGurOS
------------------

```
which perl
```

Every script starts with shebang:"#!" which is not read as a comment. The first line is actually a place where you put your interpreter, which in this case is going to be the output of the command above.

```perl
#!/usr/bin/perl
print "Hello from LiGurOS!\n";
```

Now make the script executable and you can run it.

```
liguros ~ # chmod +x hello.pl
liguros ~ # ./hello.pl
Hello from LiGurOS!
liguros ~ #
```
