---
title: "Hacking with LiGurOS"
date: 2019-11-12T18:47:41+01:00
draft: false
weight: 100
---

We are planning of adding a couple of tutorials here to get you started with coding in your favorite programming languages. The tutorials are not going to be a complete programming language resource, just something to get you started.

All tutorials are LiGurOS Linux oriented, so we will be giving you the commands necessary to set up the LiGurOS Linux environment for each language, after you have followed the installation tutorial.

Let's get started! Happy Programming!
