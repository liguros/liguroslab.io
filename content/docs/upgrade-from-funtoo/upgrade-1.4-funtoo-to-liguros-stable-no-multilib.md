---
title: 'Upgrading to Liguros from Funtoo 1.4'
date: 2019-02-11T19:27:37+10:00
draft: true
weight: 20
---

These are the steps you have to do on your installation to convert it to LiGurOS. It should take your Funtoo 1.4 installation and convert it to a no-multilib Liguros installation. I opted for the stable release here but you can also use develop as well. For that you just need to set release = develop in ego.conf.

1. Replace ego.conf

```
[global]
release = stable
sync_base_url = https://gitlab.com/liguros/{repo}.git
```

2. Remove the funtoo tree

```
rm -rf /var/git/meta-repo
```

3. Sync with new tree
```
ego sync
```

4. Update `/etc/portage/make.profile/parent` to correct values using `ego profile`, 
5. in our example we are using amd64-zen as our subarch, for the list of possible subarchs please use `epro list`:
```
epro arch amd64
epro subarch amd64-zen
epro build current
epro flavor core
epro mix-ins +no-multilib +openssl
```

Add needed python-kit settings:
```
echo "core-kit:liguros/kits/python-kit/$(cat /var/git/meta-repo/kits/core-kit/.git/HEAD |cut -f 3 -d"/")" >> /etc/portage/make.profile/parent
```

Check your `/etc/portage/make.profile/parent` content:
```
cat /etc/portage/make.profile/parent 
core-kit:liguros/1.0/linux-gnu/arch/amd64
core-kit:liguros/1.0/linux-gnu/arch/amd64/subarch/amd64-zen
core-kit:liguros/1.0/linux-gnu/build/current
core-kit:liguros/1.0/linux-gnu/flavor/core
core-kit:liguros/1.0/linux-gnu/mix-ins/no-multilib
core-kit:liguros/1.0/linux-gnu/mix-ins/openssl
core-kit:liguros/kits/python-kit/20.1-release
```

    note: The below example assumes you are using amd64 as arch and amd64-zen as subarch - if you are using x86 or other then you will need to set these instead.
    note: The release behind python-kit is the current release according to your version that is available, so don't worry if it shows something different.

6. Update ego
```
emerge -v1 --nodeps ego
```

7. Update ego
```
ego sync --config-only
```

8. Update python and deps
 
```
emerge -v1 python python-exec --nodeps
eselect python set *latest-python*
emerge -avN1 setuptools certifi cffi pycparser cryptography six --nodeps
```

9. Upgrade toolchain.
```
emerge -1av --nodeps gcc
gcc-config -l
gcc-config *new gcc*
emerge -1av --nodeps glibc
emerge -1av --nodeps binutils
binutils-config -l
binutils-config *new binutils*
env-update && source /etc/profile
```

10. Upgrade some other base/system packages
```
emerge -1av --nodeps bash
emerge -1av --nodeps portage
emerge -1av --nodeps linux-headers
```

11. Update perl
```
emerge -1av --nodeps perl
perl-cleaner --reallyall
```

12. Solve circular dependencies with pam and filecap
```
USE="-filecaps" emerge -1av --nodeps pam
emerge -1av --nodeps libcap
emerge -1av --nodeps pam
```

13. Update world
```
emerge --ask --update --verbose --emptytree --with-bdeps=y --newuse --keep-going @world
```

Due to the version differences between funtoo 1.4 and liguros it can be that the above command will give you some errors. If you are getting dependency errors, check if the complaining version is in liguros and if it is the latest version available. Sometimes you have to uninstall the offending packages so that emerge is able to resolve the dependencies correctly.

14. End

You should now be complete and up to date. Proceed to clean up any old kernel-sources, kernel-modules or dependencies.
```
emerge --ask --depclean
```

Keep it rolling,
Your LiGurOS Team
