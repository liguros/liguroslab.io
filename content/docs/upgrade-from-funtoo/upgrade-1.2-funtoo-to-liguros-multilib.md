---
title: 'Upgrading to Liguros from Funtoo 1.2'
date: 2019-02-11T19:27:37+10:00
draft: true
weight: 20
---

These are the steps you have to do on your installation to convert it to LiGurOS.

<div class=“message warning” data-component=“message”>
    <h5>Warning!</h5>
    Upgrading Funtoo 1.3 and 1.4 is possible, but involves creating binaries of multilib-capable toolchain.
    If you don't know how to do this ask someone that can or select the reinstall route.
    <span class=“close small”></span>
</div>

1. Replace ego.conf

```
[global]
release = develop
sync_base_url = https://gitlab.com/liguros/{repo}.git
```

2. Remove the funtoo tree

```
rm -rf /var/git/meta-repo
```

3. Sync with new tree
```
ego sync
```

4. Manually hack /etc/portage/make.profile/parent
```
core-kit:liguros/1.0/linux-gnu/arch/amd64
core-kit:liguros/1.0/linux-gnu/arch/amd64/subarch/generic_64
core-kit:liguros/1.0/linux-gnu/build/current
core-kit:liguros/1.0/linux-gnu/flavor/core
core-kit:liguros/1.0/linux-gnu/mix-ins/python3-only
core-kit:liguros/1.0/linux-gnu/mix-ins/openssl
core-kit:liguros/1.0/linux-gnu/mix-ins/multilib
core-kit:liguros/kits/python-kit/20.1-release
nokit:liguros/kits/python-kit/20.1-release
python-kit:liguros/kits/python-kit/20.1-release
python-modules-kit:liguros/kits/python-kit/20.1-release
```

note: If the above manual hacking of make.profile/parent gives you errors then you can do the following, which will set your arch and automatically regenerate make.profile/parent.<br />
note2: The below example assumes you are using amd64 - if you are using x86 or other then you will need to set these instead.<br />
note3: Multilib is now set via the multilib mix-in.


```
epro arch amd64
```

5. Update ego
```
emerge -v1 --nodeps ego
```

6. Update ego
```
ego sync --config-only
```

7. Upgrade toolchain.
```
emerge -1av --nodeps gcc
gcc-config -l
gcc-config *new gcc*
emerge -1av --nodeps glibc
emerge -1av --nodeps binutils
binutils-config -l
binutils-config *new binutils*
env-update && source /etc/profile
```

8. Upgrade some other base/system packages
```
emerge -1av --nodeps bash
emerge -1av --nodeps portage
emerge -1av --nodeps linux-headers
```

9. Update perl
```
emerge -1av --nodeps perl
perl-cleaner --reallyall
```

10. Solve circular dependencies with pam and filecap
```
USE="-filecaps" emerge -1av --nodeps pam
emerge -1av --nodeps libcap
emerge -1av --nodeps pam
```

11. Update world
```
emerge --ask --update --verbose --emptytree --with-bdeps=y --newuse --keep-going @world
```

12. End

You should now be complete and up to date. Proceed to clean up any old kernel-sources, kernel-modules or dependencies.
```
emerge --ask --depclean
```

Keep it rolling,
Your LiGurOS Team
