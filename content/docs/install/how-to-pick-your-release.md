---
title: 'How to pick your release'
date: 2019-11-05T19:27:37+10:00
weight: 11
---

LiGurOS is meant to be used in production, and we really try hard to test at least the most common combinations of software. We build our system base from source on a weekly basis and this should give you peace of mind to use our OS in your production environment.

Tracking a source based distribution can lead very fast to some unforeseen errors and issues. LiGurOS has you covered in this situation as well. We have two standard releases:

- develop (This is where all of our development takes place. This release is also following our upstream sources, with only a slight time delay of maximum 12 hours and is meant to be used in a staging environment or for testing new features).
- stable (This is a snapshotted version of develop. It brings stability and security with a minimal chance of breakage. This release is recommended for production environment).

To pick your release, simply go to /etc/ego.conf and edit it. Put `develop` or `stable` as your release and do a `ego sync` to get the latest changes. (Removal of the `/var/git/meta-repo` directory might be needed.)

1. Edit /etc/ego/ego.yml

```
global:
    # Release to use.
    release: stable

    # Path to the repo config.
    reposconfpath: /etc/portage/repos.conf/liguros.conf
```

2. Remove the old tree

```
rm -rf /var/git/liguros-repo
```

3. Sync with new tree
```
ego sync
```

We hope that this information is easy to understand.

Keep it rolling,
Your LiGurOS Team
