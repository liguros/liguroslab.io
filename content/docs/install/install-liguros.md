---
title: 'How to install LiGurOS'
date: 2021-10-29T11:57:37+02:00
weight: 10
---

LiGurOS is easy to install, if you have ever done a Gentoo type installation before. We are going to show you how this can be done in couple of easy steps. 
To start with you need some virtual or real hardware, where you want to install LiGurOS, and a bootable rescue medium to access that hardware from a temporary OS, which will allow you to install LiGurOS.

1. Boot into the temporary OS
2. Create partitions on the disk, you want to install LiGurOS on, and format the filesystem
```
gdisk /dev/sda
```
Create a `BIOS boot partition` of 8M size.
```
mkfs.btrfs /dev/sda2
mount /dev/sda2 /mnt
```

3. Download a LiGurOS tarball from https://build.liguros.net/ (eg. https://build.liguros.net/x86_64/develop/openssl/) - and grab the latest stage3-develop-openssl.tar.xz

```
cd /mnt
wget https://build.liguros.net/x86_64/develop/openssl/2021-10-29/stage3-develop-openssl.tar.xz
wget https://build.liguros.net/x86_64/develop/openssl/2021-10-29/stage3-develop-openssl.tar.xz.sum
sha512sum -c stage3-develop-openssl.tar.xz.sum
```
When the file validates, extract the tarball
```
tar --acls --xattrs -xaf stage3-develop-openssl.tar.xz .
```

4. Prepare and chroot into the new partition
```
mount -t proc proc proc
mount --rbind /sys sys
mount --rbind /dev dev
cp /etc/resolv.conf etc/resolv.conf
cd ..
chroot /mnt /bin/bash
ego sync
/usr/sbin/env-update
source /etc/profile
```

5. Install and configure grub and firmware
```
emerge -av grub linux-firmware
grub-install /dev/sda
grub-mkconfig > /boot/grub/grub.cfg
```

6. Change the password for the root user of the chroot environment
```
passwd
```

7. Update some files and settings
```
echo "/dev/sda2   /   btrfs   defaults    0 0" > /etc/fstab
```

8. Reboot :)

We hope that this information was helpful and you are running your LiGurOS system on your favorite hardware.

Keep it rolling,
Your LiGurOS Team
